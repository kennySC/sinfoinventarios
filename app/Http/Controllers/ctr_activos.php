<?php

namespace App\Http\Controllers;

use App\Models\Activo;
use App\Models\Institucion;
use App\Models\TipoActivo;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

class ctr_activos extends Controller
{
    public function __invoke()
    {
        $activos = Activo::all();
        /*$tipos_activos = Activo::with('tipoactivos')->get();*/
        //$instituciones = Activo::with('instituciones')->get();
    return view('activos.activos', compact('activos'/*,'tipos_activos', 'instituciones'*/));
    }

    // Metodo al dar al boton de EDITAR en un activo, es decir, con el cual se pasa a la vista de editar un activo //
    public function goEditarActivo($id) {
        
        $activo = Activo::where('codActivo', '=', $id)->first();
        if(is_null($activo)) {
            session(['c_error' => 'Activo no encontrado']);
            return view('activos.activos');
        }
        else{
            return view('activos.editarActivo', compact('activo'));
        }
    }

    // Metodo para ACTUALIZAR la informacion del activo como tal, es decir un update de la informacion del activo correspondiente //
    public function actualizarActivo(Request $req) {                
        $req->validate([
            'txt_nom_activo' => 'required',
            'txt_serie_activo' => 'required',
            'txt_valor_activo' => 'required',
            'txt_resp_activo' => 'required'
        ]);

        $activo = Activo::where('codActivo', '=', $req->codActivo)->first();
        
        $activo->idTipoActivo = $req->cmb_tipo;
        $activo->idInstitucion = $req->cmb_institucion;
        $activo->nombre = $req->txt_nom_activo;
        $activo->serie = $req->txt_serie_activo;
        $activo->valor = $req->txt_valor_activo;
        $activo->responsable = $req->txt_resp_activo;
        $activo->estado = $req->cmb_estado;
        $activo->observaciones = $req->txt_observ_activo;

        /* HAY QUE VALIDAR SI SE SUBIO UNA FOTO O NO */
        $activo->fotoActivo = 0;

        $activo->save();

        return redirect ('/activos');
    }

    // Metodo para abrir la pantalla de agregar activos
    public function agregarActivo() {
        return view('activos.agregarActivo');
    }

    /* METODO PARA GUARDAR UN NUEVO ACTIVO */
    public function guardarActivo(Request $req) {
        $req->validate([
            'txt_nom_activo' => 'required',
            'txt_cod_activo' => 'required',
            'txt_serie_activo' => 'required',
            'txt_valor_activo' => 'required',
            'txt_resp_activo' => 'required',

            'cmb_inst' => 'required',
            'cmb_tip_act' => 'required',
            'cmb_estado_activo' => 'required',
        ]);

        $activo = new Activo;
        $activo->codActivo = $req->txt_cod_activo;
        $activo->idTipoActivo = $req->cmb_tip_act;
        $activo->idInstitucion = $req->cmb_inst;
        $activo->nombre = $req->txt_nom_activo;
        $activo->serie = $req->txt_serie_activo;
        $activo->valor = $req->txt_valor_activo;
        $activo->responsable = $req->txt_resp_activo;
        $activo->estado = $req->cmb_estado_activo;
        $activo->observaciones = $req->txt_observ_activo;
        
        /* HAY QUE VALIDAR SI SE SUBIO UNA FOTO O NO */
        $activo->fotoActivo = 0;

        $activo->save();

        return redirect ('/activos');
    }

    /* METODO PARA DAR DE BAJA UN ACTIVO, ES DECIR, CAMBIARLE EL ESTADO NADA MAS */
    public function bajaActivo($codigo_activo) {
        $activo = Activo::where('codActivo', '=', $codigo_activo)->first();
        $activo->estado = "DB";
        
        $activo->save();

        return redirect ('/activos');
    }

}
