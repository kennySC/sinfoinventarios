<?php


namespace App\Http\Controllers;

use App\Models\Instrumento;
use App\Models\Activo;
use App\Models\FamiliaInstrumento;
use App\Models\TipoInstrumento;
use App\Models\Institucion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

class ctr_instrumentos extends Controller
{
    public function __invoke(){
       $instrumentos = Instrumento::with('activo','familiaInstrumento','tipoInstrumento','activo.institucion')->get();





       
       $familia = FamiliaInstrumento::All();
       $institucion = Institucion::All();
       $tipo = TipoInstrumento::All();

       return view('instrumentos.instrumentos', compact('instrumentos','familia','tipo','institucion'));
    }
}
