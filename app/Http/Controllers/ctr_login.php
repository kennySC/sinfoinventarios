<?php

namespace App\Http\Controllers;

use App\Models\Persona;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Symfony\Contracts\Service\Attribute\Required;

class ctr_login extends Controller
{
    public function __invoke()
    {
        if(!is_null(session('userdata')) && (session('userdata')['logged_in'] == true)) {
            return view('inicio.inicio');
        }
        return view('login.login');
    }

    public function ingresar(Request $req) {

        $req->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $usuario = Usuario::with('persona')->where('nombreUsuario', '=', $req->username)->where('contraseña', '=', $req->password)->first();
        if(is_null($usuario)) {
            session(['c_error' => 'Usuario o contraseña incorrectos.']);
            return redirect('/');
        } else {            
            $nomCompleto = $usuario->persona->nombre . ' ' . $usuario->persona->apellido . ' ' . $usuario->persona->segundoApellido;
            $user_data = array(
                'logged_in' => TRUE,
                'userID'    => $usuario->idUsuario,
                'username'  => $usuario->nombreUsuario,
                'realname'  => $nomCompleto,
                'userPic'   => $usuario->fotoUsuario,
                'role'  => $usuario->rolAcceso,
            );
            session(['userdata' => $user_data]);
            return view('inicio.inicio');
        }
    }

    public function salir(){
        session()->forget('userdata');
        session()->forget('c_error');
        return view('login.login');
    }

}