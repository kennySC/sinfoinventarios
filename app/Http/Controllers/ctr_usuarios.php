<?php


namespace App\Http\Controllers;


use App\Models\Persona;
use App\Models\Usuario;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

class ctr_usuarios extends Controller
{
    public function __invoke()
    {   
        $usuarios = Usuario::with('persona')->get();

        return view('usuarios.usuarios', compact('usuarios'));
    }    

    // Metodo para abrir la pantalla de agregar usuarios
    public function agregarUsuario() {
        return view('usuarios.agregarUsuario');
    }

    // Metodo para guardar un usuario nuevo
    public function guardarUsuario(Request $req) {
        $req->validate([
            'txt_ced' => 'required',
            'txt_nombreUs' => 'required',
            'txt_password' => 'required',
            'txt_nombre' => 'required',
            'txt_apellido' => 'required',
            'txt_apellido2' => 'required'
        ]);

        $persona = new Persona();
        $usuario = new Usuario();

        $persona->cedula = $req->txt_ced;
        $persona->nombre = $req->txt_nombre;
        $persona->segundoNombre = $req->txt_nombre2;
        $persona->apellido = $req->txt_apellido;
        $persona->segundoApellido = $req->txt_apellido2;
        $persona->genero = 'M';
        $persona->tipoPersona = 'Us';

        $persona->save();        
        
        $usuario->cedulaPersona = $req->txt_ced;
        $usuario->nombreUsuario = $req->txt_nombreUs;
        $usuario->contraseña = $req->txt_password;
        $usuario->rolAcceso = $req->cmb_rol;
        $usuario->estado = 'A';

        $usuario->save();

        return redirect('/usuarios');

    }

    // Metodo para abrir la pantalla de editar usuarios
    public function editarUsuario($id) {

        $usuario = Usuario::with('persona')->where('idUsuario', '=', $id)->first();
        if(is_null($usuario)) {
            session(['c_error' => 'Usuario no encontrado']);
            return view('usuarios.usuarios');
        } else {
            return view('usuarios.editarUsuario', compact('usuario'));
        }
        
    }

    // metodo para actualizar la informacion del usuario editado
    public function actualizarUsuario(Request $req) {

        $req->validate([
            'txt_nombreUs' => 'required',
            'txt_password' => 'required',
            'txt_nombre' => 'required',
            'txt_apellido' => 'required',
            'txt_apellido2' => 'required'
        ]);

        $usuario = Usuario::with('persona')->where('idUsuario', '=', $req->idUsuario)->first();
        echo $usuario->idUsuario;
        $persona = Persona::where('cedula', '=', $usuario->cedulaPersona)->first();  

        $usuario->nombreUsuario = $req->txt_nombreUs;
        $usuario->contraseña = $req->txt_password;
        $usuario->rolAcceso = $req->cmb_rol;
        
        $usuario->save();
                                      
        $persona->nombre = $req->txt_nombre;
        $persona->segundoNombre = $req->txt_nombre2;
        $persona->apellido = $req->txt_apellido;
        $persona->segundoApellido = $req->txt_apellido2;

        $persona->save();

        return redirect('/usuarios');
    }    

    // Metodo para dar de baja un usuario
    public function bajaUsuario($id) {

        $usuario = Usuario::with('persona')->where('idUsuario', '=', $id)->first();
        $usuario->estado = 'I';
        $usuario->save();

        return redirect('/usuarios');

    }

}
