<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instrumento extends Model
{
    use HasFactory;

    protected $table = "instrumentos";
    protected $primaryKey = 'idInstrumento';
    public $timestamps = false;

    public function activo(){
        return $this->belongsTo(Activo::class, 'codigoActivo', 'codActivo');
    }
    public function familiaInstrumento(){
        return $this->belongsTo(FamiliaInstrumento::class, 'idFamilia', 'idFamilia');
    }
    public function tipoInstrumento(){
        return $this->belongsTo(TipoInstrumento::class, 'idTipoInstrumento', 'idTipoInstrumento');
    }
    public function institucion() {
        return $this->activo()->intitucion();
    }

}
