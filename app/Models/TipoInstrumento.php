<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoInstrumento extends Model
{
    use HasFactory;
    protected $table = "tipoinstrumento";
    protected $primaryKey = 'idTipoInstrumento';
    public $timestamps = false;
}

