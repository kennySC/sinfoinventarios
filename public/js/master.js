window.addEventListener('load', cargar, false);


function cargar() {
    
}

function mostrarMenuUsuario() {
    var menu = document.getElementById("div_menuUsuario");
    if (menu.className == "cerrado") {
        menu.style.animation = "abrirMenuUsuario 500ms both";
        menu.className = "abierto";
    } else if(menu.className == 'abierto') {
        menu.style.animation = "cerrarMenuUsuario 500ms both";
        menu.className = "cerrado";
    }
}

function mostrarMenu(x) {    
    x.classList.toggle("cambiar");
    if(x.id == 'cerrado'){
        x.style.animation = "abrirBoton 1s both";
        x.id = 'abierto';
    } else {
        x.style.animation = "cerrarBoton 1s both";
        x.id = 'cerrado';
    }        
    var menu = document.getElementById("div_menu");
    if(menu.className == 'cerrado') {
        menu.style.animation = "abrirMenu 1s both";
        menu.className = 'abierto';
    } else {
        menu.style.animation = "cerrarMenu 1s both";
        menu.className = 'cerrado';
    }
}