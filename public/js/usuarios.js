

//  Metodo para mostrar la ventana modal para dar de baja
function darDeBaja(boton) {
    var id = boton.id;
    var nombreUsuario = boton.name;    

    document.getElementById('div_fondoOscuro').style = 'visibility: visible';
    document.getElementById('div_contMensaje').innerHTML = 
                                "<span id='span_mensajeBaja'>" +
                                    "Desea dar de baja al usuario: <label id='span_nombre'> " + nombreUsuario + "</label>" +
                                "</span>" +
                                "<div id='div_botonesBaja'>" +
                                    "<a class='btnsBaja' id='btn_aceptarBaja' href='http://sinfo.test/usuarios/darBaja/" + id + "'>Acepar</a>" +
                                    "<button class='btnsBaja' id='btn_cancelBaja' onclick='cerrarBaja()'>Cancelar</button>" +
                                "</div>";
    
}

//  Metodo para cancelar la dada de baja
function cerrarBaja() {
    document.getElementById('div_fondoOscuro').style = 'visibility: hidden';
}

