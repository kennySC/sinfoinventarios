@extends('layouts.master')

@section('css_js')
    <link rel="stylesheet" href="/css/activos.css">
    <script src="/js/activos.js"></script>
@endsection

@section('content')

    <div class="div_contenido">
    <!--Div del contenido del LADO IZQUIERDO de la vista, es decir, lo que respecta lo de filtrar y el boton para agregar un nuevo activo-->
        <div id="div_izq">
            @csrf
            <!--FORM donde vendran todas las opciones posibles de filtrado-->
            <form id="form_filtrar" action="" method="GET">
                <label id="lb_buscar" for="txt_nom_activo">
                    Nombre del Activo:
                    <input type="text" name="txt_nom_activo" id="txt_nom_activo" class="txt_datos">
                </label>
                <label id="lb_buscar" for="txt_cod_activo">
                    Código del Activo:
                    <input type="text" name="txt_cod_activo" id="txt_cod_activo" class="txt_datos">
                </label>
                <label id="lb_buscar" for="txt_serie_activo">
                    # Serie del Activo:
                    <input type="text" name="txt_serie_activo" id="txt_serie_activo" class="txt_datos">
                </label>
                <label id="lb_buscar" for="txt_valor_activo">
                    Valor $:
                    <input type="number" name="txt_valor_activo" id="txt_valor_activo" class="txt_datos">
                </label>

                <div id="div_opciones">
                    <!--Div donde podra escoger a cual institucion pertenece el activo-->
                    <div id="div_institucion">
                        Institución <br>
                        <!--Aqui tiene que ir el foreach para las instituciones que hay foreach($instituciones ?? '' as $inst)-->
                        <!--<label class="lb_contenedor">   
                            <input type="radio" name="rb_inst"> inst->nombre
                        </label>-->
                        <label class="lb_contenedor">   
                            <input type="radio" name="rb_inst"> UNA
                        </label>
                        <label class="lb_contenedor">
                            <input type="radio" name="rb_inst"> AEMS
                        </label>
                        <label class="lb_contenedor">
                            <input type="radio" name="rb_inst"> SINEM
                        </label>
                        <label class="lb_contenedor">
                            <input type="radio" name="rb_inst"> MPZ
                        </label>
                        <!--Radio button de opcion para mostrar los activos de cualquier institucion-->
                        <label class="lb_contenedor">
                            <input type="radio" name="rb_inst"> Todos
                        </label>
                        <!--SE CIERRA EL FOR EACH-->
                    </div>
                    <!--Div donde se podra escoger por medio de radio button que tipo de activo-->
                    <div id="div_tipo_activo">
                        Tipo de Activo <br>
                        <!--Hacemos un FOR EACH para obtener de la base de datos todos los posibles tipos de activos que hay-->
                        <!--Aqui tiene que ir el foreach para los tipos de activos que hay@foreach($tipos_activos ?? '' as $tip_act)-->
                            <!--<label class="lb_contenedor">   
                                <input type="radio" name="rb_tip_act"> tip_act->tipoActivo
                            </label>-->
                            <label class="lb_contenedor">   
                                <input type="radio" name="rb_tip_act"> Cómputo
                            </label>
                            <label class="lb_contenedor">   
                                <input type="radio" name="rb_tip_act"> Electrodoméstico
                            </label>
                            <label class="lb_contenedor">   
                                <input type="radio" name="rb_tip_act"> Mobiliario
                            </label>
                            <label class="lb_contenedor">   
                                <input type="radio" name="rb_tip_act"> Audio
                            </label>
                            <label class="lb_contenedor"> 
                                <input type="radio" name="rb_tip_act"> Todos
                            </label>
                        <!-- Aqui hay que terminar el foreach@endforeach-->
                    </div>
                    <!--Div donde se podra escoger por medio de radio button el estado del activo-->
                    <div id="div_estados">
                        Estado  <br>
                        <label class="lb_contenedor">
                            <input type="radio" name="rb_estado"> En Reparación           
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Dado de Baja                      
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> En uso                      
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Bodega (Buen estado)
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Bodega (Mal estado)           
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Contrato
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Boleta de Salida
                        </label>
                        <!--Radio button de opcion para mostrar los activos con cualquier estado-->
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Todos
                        </label>
                    </div>
                    <input type="submit" class="boton" id="btn_filtrar" value="Filtrar">
                </div>
            </form>

            <!--BOTON AGREGAR NUEVO ACTIVO--> 
            <a id="btn_nuevo_activo" class="boton" href="{{route('agregarActivo')}}" >Agregar Activo</a>

        </div>

        <!--Div del contenido del LADO DERECHO de la vista, es decir, lo que respecta los activos como tal, junto con sus botones de editar y dar de baja-->
        <div id="div_der">
            <!--Aqui viene el FOREACH para mostrar todos los activos en el sistema-->
            @foreach ($activos ?? '' as $act)
                <div class="div_cont_activo"> <!--Div contenedor activo-->
                    <h1>{{$act->nombre}}</h1>
                        <div class="div_datos_activo"><!--Div donde iran los datos del activo, la info y foto en caso de tener-->
                            <div class="div_foto_activo">
                                <!--Consultamos si el activo cuenta con foto, de ser asi, se carga-->
                                @if ($act->fotoActivo == 1)                         
                                    <img class="img_fotoAct" src="/rsc/userPics/LLAVES$act->nombreLLAVES.png" alt="">
                                <!--De no tener, cargamos un icono predeterminado-->
                                @else
                                    <!--Si el activo no cuenta con foto consultamos el tipo de activo para cargar una imagen por defecto de acuerdo a su tipo-->
                                    @switch($act->idTipoActivo)
                                        @case('1') <!--CASO 1 ACTIVO MOBILIARIO-->
                                            <img class="img_fotoAct" src="/rsc/pngs/mobiliario.png" alt="">
                                        @break
                                        @case('2') <!--CASO 2 ACTIVO COMPUTOI-->
                                            <img class="img_fotoAct" src="/rsc/pngs/computo.png" alt="">
                                        @break
                                        @case('3') <!--CASO 3 ACTIVO AUDIO-->
                                            <img class="img_fotoAct" src="/rsc/pngs/audio.png" alt="">
                                        @break
                                        @case('4') <!--CASO 4 ACTIVO ELECTRODOMESTICO-->
                                            <img class="img_fotoAct" src="/rsc/pngs/electrodomestico.png" alt="">
                                        @break
                                    @endswitch
                                @endif
                            </div> 
                            <!--Div con la informacion respectiva del activo-->   
                            <div class="div_info_activo">
                                Nombre: 
                                <span> {{$act->nombre}} </span> 
                                <br>
                                Tipo: 
                                <span> {{$act->idTipoActivo}} </span> 
                                <br>
                                Institucion: 
                                <span> {{$act->idInstitucion}} </span> 
                                <br>
                                Codigo: 
                                <span> {{$act->codActivo}} </span> 
                                <br>
                                # Serie: 
                                <span> {{$act->serie}} </span> 
                                <br>
                                Valor: 
                                <span> ${{$act->valor}} </span>
                                <br>
                                Responsable: 
                                <span> {{$act->responsable}} </span>
                                <br>
                                Estado: 
                                    @switch ($act->estado)
                                        @case('RP')
                                            <span> En reparación </span> 
                                        @break
                                        @case('DB')
                                            <span style="color:red"> Dado de baja </span> 
                                        @break
                                        @case('EU')
                                            <span> En uso </span> 
                                        @break
                                        @case('EBB')
                                            <span> En bodega, buen estado </span> 
                                        @break
                                        @case('EBM')
                                            <span> En bodega, mal estado </span> 
                                        @break
                                        @case('ECP')
                                            <span> En contrato de préstamo </span> 
                                        @break
                                        @case('EBS')
                                            <span> En boleta de salida </span> 
                                        @break
                                    @endswitch
                                <br>
                                Observaciones: 
                                <span> {{$act->observaciones}} </span> 
                                <br>
                            </div>
                        </div>
                        <!--Div para los botones de EDITAR Y DAR DE BAJA para cada activo -->
                        <div class="div_botones">
                            <a class="btnEditar" href="{{route('goEditarActivo', $act->codActivo)}}">Editar</a> 
                            <input type="button" class="btnBaja" id="{{$act->codActivo}}" name='{{$act->nombre}}' value="Dar de Baja" onclick="darDeBaja(this)">
                        </div>
                </div>
            @endforeach
        </div>
        <div id="div_fondoOscuro">
            <div id="div_contMensaje">
                
            </div>
        </div>
    </div>

@endsection