@extends('layouts.master')

@section('title', 'Agregar Activo')

@section('css_js')
    <link rel="stylesheet" href="/css/editarActivo.css">
@endsection

@section('content')

    <div class="div_contenido">
        <div class="div_info_activo">
            <form id="form_activo" action="{{route('guardarActivo')}}" method ="post">
                @csrf
                <div id="div_datos">
                    <label for="txt_nom_activo">
                        Nombre del activo: <br>
                        <input class="txt" type="text" name="txt_nom_activo" id="txt_nom_activo" value="{{old('txt_nom_activo')}}">
                        @error('txt_nom_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_cod_activo">
                        Código del activo: <br>
                        <input class="txt" type="text" name="txt_cod_activo" id="txt_cod_activo" value="{{old('txt_cod_activo')}}">
                        @error('txt_cod_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_serie_activo">
                        # Serie del activo: <br>
                        <input class="txt" type="text" name="txt_serie_activo" id="txt_serie_activo" value="{{old('txt_serie_activo')}}">
                        @error('txt_serie_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_valor_activo">
                        Valor $ del activo: <br>
                        <input type="number" name="txt_valor_activo" id="txt_valor_activo" value="{{old('txt_valor_activo')}}">
                        @error('txt_valor_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_resp_activo">
                        Responsable del activo: <br>
                        <input type="text" name="txt_resp_activo" id="txt_resp_activo" value="{{old('txt_resp_activo')}}">
                        @error('txt_resp_activo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <!--COMBOBOX para la INSTITUCION-->
                    <label for="cmb_institucion">
                    Institución: <br>
                        <select name="cmb_institucion" id="cmb_institucion">
                            <option value="1">UNA</option>
                            <option value="2">MPZ</option>
                            <option value="3">AEMS</option>
                            <option value="4">SINEM</option>
                        </select>
                        @error('cmb_institucion')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <!--COMBOBOX para el TIPO DE ACTIVO-->
                    <label for="cmb_tipo">
                    Tipo de Activo: <br>
                        <select name="cmb_tipo" id="cmb_tipo">
                            <option value="1">Mobiliario</option>
                            <option value="2">Computo</option>
                            <option value="3">Audio</option>
                            <option value="4">Electrodomestico</option>
                        </select>
                        @error('cmb_tipo')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <!--COMBOBOX para el ESTADO DEL ACTIVO-->
                    <label for="cmb_estado">
                    Estado: <br>
                        <select name="cmb_estado" id="cmb_estado">
                            <option value="RP">En reparacion</option>
                            <option value="DB">Dado de baja</option>
                            <option value="EU">En uso</option>
                            <option value="EBB">En bodega, buen estado</option>
                            <option value="EBM">En bodega, mal estado</option>
                            <option value="ECP">En contrato de préstamo</option>
                            <option value="EBS">En boleta de salida</option>
                        </select>
                        @error('cmb_estado')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </label>
                    <label for="txt_observ_activo">
                        Observaciones: <br>
                        <input class="txt_obs" type="text" name="txt_observ_activo" id="txt_observ_activo" value="{{old('txt_observ_activo')}}">
                    </label>
                    @error('txt_observ_activo')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                    <!--Div para editar en caso de ser deseado la FOTO del activo-->
                    <div class="div_foto_act" id="div_foto_act">
                        <h1>Foto:</h1>
                        
                        
                        
                        <!--BOTON PARA SUBIRLE UNA FOTO AL ACTIVO-->
                        <a id="btn_foto_activo" class="btn" >ICONO DE UNA CAMARA</a>
                    </div>
                    <input type="hidden" name="codActivo">
                </div>
                <div id="div_btns">
                    <input class="btn" type="submit" id="btn_guardar" value="Guardar">
                    <input class="btn" type="button" id="btn_cancelar" value="Cancelar" onclick="cancelar()">
                    <!--<a href="{{route('activos')}}" id="a_cancelar"></a>-->
                </div>
            </form>
        </div>
    </div>
@endsection