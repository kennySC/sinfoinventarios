@extends('layouts.master')

@section('title', 'Inicio SinfoInventarios')

@section('css_js')
    <link rel="stylesheet" href="/css/inicio.css">
    <script src="/js/inicio.js"></script>
@endsection

@section('content')

    <div class="div_contenido">  
        <h2><script>saludo()</script></h2>
        <div id="div_instrumentos"></div>
    </div>    

@endsection