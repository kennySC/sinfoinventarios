@extends('layouts.master')

@section('css_js')
    <link rel="stylesheet" href="/css/instrumentos.css">
@endsection

@section('content')
<div class="div_contenido">
        <div id="div_izq">
            <div>
            @csrf
            <form id="form_filtrar" action="" method="GET">
                
                <label id="lb_buscar" for="txt_codigo_instrumento">
                    Código del Instrumento:
                    <input type="text" name="txt_codigo_instrumento" id="txt_codigo_instrumento" class="txt_datos">
                </label>
                <label id="lb_buscar" for="txt_serie_instrumento">
                    # Serie del Instrumento:
                    <input type="text" name="txt_serie_instrumento" id="txt_serie_instrumento" class="txt_datos">
                </label>
                <label id="lb_buscar" for="txt_valor_instrumento">
                    Valor $:
                    <input type="number" name="txt_valor_instrumento" id="txt_valor_instrumento" class="txt_datos">
                </label>
                <label id="lb_buscar" for="txt_modelo_instrumento">
                    Modelo del Instrumento:
                <input type="text" name="txt_modelo_instrumento" id="txt_modelo_instrumento" class="txt_datos">
                </label>
                <label id="lb_buscar" for="txt_marca_instrumento">
                    Marca del Instrumento:
                <input type="text" name="txt_marca_instrumento" id="txt_marca_instrumento" class="txt_datos">
                </label>
                <label id="lb_buscar" for="txt_responsable_instrumento">
                    Responsable del Instrumento:
                <input id="lb_buscar" type="text" name="txt_responsable_instrumento" id="txt_responsable_instrumento" class="txt_datos">
                
                <label id="lb_buscar" for="slc_tipo_instrumentos">
                    Tipo de Instrumento:
                    <select name="slc_tipo_instrumentos" id="slc_tipo_instrumentos" class= 'slc_datos'>
                    @foreach ($tipo ?? '' as $ti)
                        <option value='{{$ti->idTipoInstrumento}}'>{{$ti->tipoInstrumento}}</option>
                    @endforeach
                    </select> 
                 </label>
                
                <label id="lb_buscar" for="slc_familia_instrumentos" >
                    Familia del Instrumento:
                    <select name="slc_familia_instrumentos" id="slc_familia_instrumentos" class= 'slc_datos'>
                    @foreach ($familia ?? '' as $fa)
                        <option value='{{$fa->idFamilia}}'>{{$fa->familiaInstrumento}}</option>
                    @endforeach
                    </select> 
                </label>

                <label id="lb_buscar" for="slc_institucion_instrumentos" >
                    Institución:
                    <select name="slc_institucion_instrumentos" id="slc_institucion_instrumentos" class= 'slc_datos'>
                    @foreach ($institucion ?? '' as $in)
                        <option value='{{$in->idInstitucion}}'>{{$in->nombre}}</option>
                    @endforeach
                    </select> 
                </label>
               
            </label>

                <div id="div_opciones">
                  <div id="div_estados">
                        Estado  <br>
                        <label class="lb_contenedor">
                            <input type="radio" name="rb_estado"> En Reparación           
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Dado de Baja                      
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> En uso                      
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Bodega (Buen estado)
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Bodega (Mal estado)           
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Contrato
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Boleta de Salida
                        </label>
                        <label class="lb_contenedor"> 
                            <input type="radio" name="rb_estado"> Todos
                        </label>
                    </div>
                    <input type="submit" class="boton" id="btn_filtrar" value="Filtrar">
                </div>
            </form>

            <a id="btn_nuevo_instrumento" class="boton" href=""  data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar Instrumento</a>
            </div>
        </div>
        <div id="div_der">
           
            @foreach ($instrumentos ?? '' as $in)
                <div class="div_cont_activo"> 
                    <h1>{{$in->activo->nombre}}</h1>
                        <div class="div_datos_activo">
                            <div class="div_foto_activo">
                                <!-----IMAGEN-------->                 
                                    <img class="img_fotoAct" src="/rsc/userPics/png" alt="">
 
                            </div> 
                            <div class="div_info_instrumento">
                                Numero: 
                                <span> {{$in->numeroInstrumento}} </span> 
                                <br>
                                Familia: 
                                <span> {{$in->familiaInstrumento-> familiaInstrumento }} </span> 
                                <br>
                                Tipo: 
                                <span> {{$in->tipoInstrumento-> tipoInstrumento }} </span> 
                                <br>
                                Nombre: 
                                <span> {{$in->activo->nombre}} </span> 
                                <br>
                                Institucion: 
                                <span> {{$in->activo->institucion->nombre }} </span> 
                                <br>
                                Codigo: 
                                <span> {{$in->activo->codActivo}} </span> 
                                <br>
                                # Serie: 
                                <span> {{$in->activo->serie}} </span> 
                                <br>
                                Valor: 
                                <span> ${{$in->activo->valor}} </span>
                                <br>
                                Responsable: 
                                <span> {{$in->activo->responsable}} </span>
                                <br>
                                Estado: 
                                    @switch ($in->activo->estado)
                                        @case('RP')
                                            <span> En reparación </span> 
                                        @break
                                        @case('DB')
                                            <span> Dado de baja </span> 
                                        @break
                                        @case('EU')
                                            <span> En uso </span> 
                                        @break
                                        @case('EBB')
                                            <span> En bodega, buen estado </span> 
                                        @break
                                        @case('EBM')
                                            <span> En bodega, mal estado </span> 
                                        @break
                                        @case('ECP')
                                            <span> En contrato de préstamo </span> 
                                        @break
                                        @case('EBS')
                                            <span> En boleta de salida </span> 
                                        @break
                                    @endswitch
                                <br>
                                Caracteristicas: 
                                <span> {{$in->caracteristica}} </span> 
                                <br>
                                Observaciones: 
                                <span> {{$in->activo->observaciones}} </span> 
                                
                                <br>
                            </div>
                        </div>
                        
                        <div class="div_botones">
                            <a class="btnEditar" href="">Editar</a> 
                            <a class="btnBaja" href="">Dar de Baja</a>
                        </div>
                </div>
            @endforeach
        </div>
        
</div>


@endsection