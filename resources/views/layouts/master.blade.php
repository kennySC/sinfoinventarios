<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/master.css">
    <script src="/js/master.js"></script>
    <title>@yield('title')</title>    
    <!-- Yields para los js y css de las diferentes vistas-->
    @yield('css_js')    

</head>
<body>
    <!-- Codigo para la barra de usuario y el icono del sistema -->
    <div id="div_top">
        <div id="div_logo">
            <img src="/rsc/imgs/logo_sis.png" alt="">
        </div>
        <div id="div_usuario" onclick="mostrarMenuUsuario()">            
            <p>{{session('userdata')['realname']}}</p>
            <div id="div_foto">
                @if (session('userdata')['userPic'] == 1)
                    <img src="/rsc/userPics/{{session('userdata')['username']}}.png">
                @else
                    <img src="/rsc/pngs/user_white.png">
                @endif
            </div>            
        </div>        
    </div>    
    <div id="div_base">        
        @yield('content')
    </div>
    <div id="div_menuUsuario" class="cerrado">
        <a href="{{route('editarUsuario', session('userdata')['userID'])}}">Editar</a>
        <a href="">Informacion</a>
        <a href="{{route('salir')}}">Cerrar Sesión</a>
    </div>     
    <div id="div_menu" class="cerrado">
        <a href="{{route('inicio')}}">Inicio</a>
        @if (session('userdata')['role'] == 'A')            
            <a href="">Estudiantes</a>
            <a href="{{route('activos')}}">Activos</a>
            <a href="{{route('instrumentos')}}">Instrumentos</a>        
            <a href="">Contratos de Prestamo</a>
            <a href="">Boletas de Salida</a>
            <a href="">Recepcion de Instrumentos</a>
            <a href="">Recepcion de Activos</a>
            <a href="">Reportes</a>        
            <a href="{{route('usuarios')}}">Usuarios</a>
        @else <!-- A esto podran acceder los demas roles -->
            <a href="{{route('activos')}}">Activos</a>
            <a href="{{route('instrumentos')}}">Instrumentos</a>     
            <a href="">Contratos de Prestamo</a>
            <a href="">Boletas de Salida</a>
            <a href="">Recepcion de Instrumentos</a>
            <a href="">Recepcion de Activos</a>
        @endif
        <a href="{{route('inicio')}}">Ayuda</a>
    </div>
    <div class="btn_menu" id="cerrado" onclick="mostrarMenu(this)">
        <div id="barra1"></div>
        <div id="barra2"></div>
        <div id="barra3"></div>
    </div>  
</body>
<script>
    
</script>
</html>