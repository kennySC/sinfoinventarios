<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/login.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <title>Sinfoinventarios</title>
</head>
<body>    
    @php
        if (session()->has('c_error')) {
            echo "<div class='msgError'>" . session('c_error') . "</div>";
            session()->forget('c_error');
        }    
    @endphp    
    <div id="div_base">
        <img id="img_logo" src="rsc/imgs/logo_sis.png">
        <div class="div_mitad">
            <img id="img_logo_emspz" src="rsc/imgs/logo.png">
        </div>
        <div class="div_mitad">
            <form id="form_login" action="{{route('ingresar')}}" method="post">
                @csrf   
                <div id="div_txts">                
                    <div class="iconoInput">
                        <input type="text" class="text" name="username" id="txt_username" placeholder="Nombre de Usuario" value="{{old('username')}}">
                        <i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i>             
                        @error('username')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </div>
                    <div class="iconoInput">
                        <input type="password" class="text" name="password" id="txt_password" placeholder="Contraseña" value="{{old('password')}}">
                        <i class="fa fa-lock fa-lg fa-fw" aria-hidden="true"></i>
                        @error('password')
                            <small class="txtError">*{{$message}}</small>
                        @enderror
                    </div>                                        
                </div>
                <div id="div_btns">
                    <input type="submit" class="btn" id="btn_login" value="Ingresar">
                    <input type="button" class="btn" id="btn_recover" value="¿Olvidó su contraseña?">
                </div>    
            </form>
        </div>
    </div>      
</body>

</html>