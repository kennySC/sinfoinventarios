@extends('layouts.master')

@section('title', 'Agregar Usuario')

@section('css_js')
    <link rel="stylesheet" href="/css/editarUsuario.css">
    <script src="/js/editarAgregarUsuarios.js"></script>
@endsection

@section('content')

<div class="div_contenido">
    <div id="div_infoUsuario">
        <form id="form_usuario" action="{{route('guardarUsuario')}}" method="post">
            @csrf
            <div id="div_txts">
                <label for="txt_nombreUS">
                    Nombre de Usuario: <br>
                    <input class="txt" type="text" name="txt_nombreUs" id="txt_nombreUs" value="{{old('txt_nombreUs')}}">
                    @error('txt_nombreUs')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_password">
                    Contraseña: <br>
                    <input class="txt" type="password" name="txt_password" id="txt_password" value="{{old('txt_password')}}">
                    @error('txt_password')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_ced">
                    Cedula: <br>    
                    <input class="txt" type="text" name="txt_ced" id="txt_ced" value="{{old('txt_ced')}}">
                    @error('txt_ced')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_nombre">
                    Nombre: <br>    
                    <input class="txt" type="text" name="txt_nombre" id="txt_nombre" value="{{old('txt_nombre')}}">
                    @error('txt_nombre')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_nombre2">
                    Segundo Nombre: <br>    
                    <input class="txt" type="text" name="txt_nombre2" id="txt_nombre2" value="{{old('txt_nombre2')}}">
                    @error('txt_nombre2')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_apellido">
                    Primer Apellido: <br>    
                    <input class="txt" type="text" name="txt_apellido" id="txt_apellido" value="{{old('txt_apellido')}}">
                    @error('txt_apellido')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_apellido2">
                    Segundo Apellido: <br>    
                    <input class="txt" type="text" name="txt_apellido2" id="txt_apellido2" value="{{old('txt_apellido2')}}">
                    @error('txt_apellido2')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="cmb_rol">
                    Rol de Usuario: <br>
                    <select name="cmb_rol" id="cmb_rol">
                        <option value="A">Administrador</option>
                        <option value="R">Regular</option>
                        <option value="M">Mantenimiento</option>
                    </select>
                </label>                
                <input type="hidden" name="idUsuario">
            </div>
            <div id="div_btns">
                <input class="btn" type="submit" id="btn_guardar" value="Guardar">
                <input class="btn" type="button" id="btn_cancelar" value="Cancelar" onclick="cancelar()">
                <a href="{{route('usuarios')}}" id="a_cancelar"></a>
            </div>
        </form>
    </div>
</div>

@endsection