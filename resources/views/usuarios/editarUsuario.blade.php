@extends('layouts.master')

@section('title', 'Editar Usuario')

@section('css_js')
    <link rel="stylesheet" href="/css/editarUsuario.css">
    <script src="/js/editarAgregarUsuarios.js"></script>
@endsection

@section('content')

<div class="div_contenido">
    <div id="div_infoUsuario">
        <form id="form_usuario" action="{{route('actualizarUsuario')}}" method="post">
            @csrf
            @method('put')
            <div id="div_txts">
                <label for="txt_nombreUS">
                    Nombre de Usuario: <br>
                    <input class="txt" type="text" name="txt_nombreUs" id="txt_nombreUs" value="{{$usuario->nombreUsuario}}">
                    @error('txt_nombreUs')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_password">
                    Contraseña: <br>
                    <input class="txt" type="password" name="txt_password" id="txt_password">
                    @error('txt_password')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_ced">
                    Cedula: <br>    
                    <input class="txt" type="text" name="txt_ced" id="txt_ced" disabled value="{{$usuario->persona->cedula}}">
                    @error('txt_ced')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_nombre">
                    Nombre: <br>    
                    <input class="txt" type="text" name="txt_nombre" id="txt_nombre" value="{{$usuario->persona->nombre}}">
                    @error('txt_nombre')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_nombre2">
                    Segundo Nombre: <br>    
                    <input class="txt" type="text" name="txt_nombre2" id="txt_nombre2" value="{{$usuario->persona->segundoNombre}}">
                    @error('txt_nombre2')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_apellido">
                    Primer Apellido: <br>    
                    <input class="txt" type="text" name="txt_apellido" id="txt_apellido" value="{{$usuario->persona->apellido}}">
                    @error('txt_apellido')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="txt_apellido2">
                    Segundo Apellido: <br>    
                    <input class="txt" type="text" name="txt_apellido2" id="txt_apellido2" value="{{$usuario->persona->segundoApellido}}">
                    @error('txt_apellido2')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>
                <label for="cmb_rol">
                    Rol de Usuario: <br>
                    <select name="cmb_rol" id="cmb_rol">
                        <option value="A" @if ($usuario->rolAcceso == 'A')
                            selected
                        @endif>Administrador</option>
                        <option value="R" @if ($usuario->rolAcceso == 'R')
                            selected
                        @endif>Regular</option>
                        <option value="M" @if ($usuario->rolAcceso == 'M')
                            selected
                        @endif>Mantenimiento</option>
                    </select>
                    @error('cmb_rol')
                        <small class="txtError">*{{$message}}</small>
                    @enderror
                </label>                
                <input type="hidden" name="idUsuario" value="{{$usuario->idUsuario}}">
            </div>
            <div id="div_btns">
                <input class="btn" type="submit" id="btn_guardar" value="Guardar">
                <input class="btn" type="button" id="btn_cancelar" value="Cancelar" onclick="cancelar()">
                <a href="{{route('usuarios')}}" id="a_cancelar"></a>
            </div>
        </form>
    </div>        
</div>

@endsection