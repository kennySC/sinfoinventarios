@extends('layouts.master')

@section('css_js')
    <link rel="stylesheet" href="/css/usuarios.css">
    <script src="/js/usuarios.js"></script>
@endsection


@section('content')

    <div class="div_contenido">
        <div id="div_izq">
            <div>
                @csrf
                <form id="form_filtrar" action="" method="GET">
                    <br>
                    <label id="lb_buscar" for="txt_buscar">
                        Nombre Real:
                        <input type="text" name="txt_buscar" id="txt_buscar">
                    </label>
                    <div id="div_opciones">
                        <div id="div_roles">
                            Rol de Acceso <br>
                            <label class="lb_contenedor">   
                                <input type="radio" name="rb_rol"> Administrador            
                            </label>
                            <label class="lb_contenedor">
                                <input type="radio" name="rb_rol"> Regular                        
                            </label>
                            <label class="lb_contenedor">
                                <input type="radio" name="rb_rol"> Mantenimiento              
                            </label>
                            <label class="lb_contenedor">
                                <input type="radio" checked="checked" name="rb_rol"> Todos
                            </label>
                        </div>
                        <div id="div_estados">
                            Estado  <br>
                            <label class="lb_contenedor">
                                <input type="radio" name="rb_estado"> Activo           
                            </label>
                            <label class="lb_contenedor"> 
                                <input type="radio" name="rb_estado"> Dado de Baja                      
                            </label>
                            <label class="lb_contenedor"> 
                                <input type="radio" checked="checked" name="rb_estado"> Todos              
                            </label>
                        </div>                    
                    </div>
                </form>
                <input type="button" class="boton" id="btn_filtrar" value="Filtrar" onclick="filtrarUsuarios(this.value)">
            </div>
            <a id="btn_nuevoUs" class="boton" href="{{route('agregarUsuario')}}">Agregar nuevo usuario</a>
        </div>
        <div id="div_der">
            @foreach ($usuarios ?? '' as $us)
                <div class="div_contenedorUsuario">
                    <h1>{{$us->nombreUsuario}}</h1>
                    <div class="div_textUsuario">
                        <div class="div_fotoUsuario">
                            @if ($us->fotoUsuario == 1)                         
                                <img class="img_fotoUs" src="/rsc/userPics/{{$us->nombreUsuario}}.png" alt="">
                            @else
                                <img class="img_fotoUs" src="/rsc/pngs/user_white.png" alt="">
                            @endif
                        </div>
                        <div class="div_infoUsuario">
                            Nombre: 
                            <span>
                                {{$us->persona->nombre}} {{$us->persona->segundoNombre}} {{$us->persona->apellido}} {{$us->persona->segundoApellido}}
                            </span>
                            <br>
                            Ced:
                            <span> {{$us->persona->cedula}}</span>
                            <br>
                            Rol de usuario:
                            <span> @switch($us->rolAcceso)
                                @case('A')
                                    Administrador
                                    @break
                                @case('R')
                                    Regular
                                    @break
                                @case('M')
                                    Mantenimiento
                                    @break
                            @endswitch</span>
                            <br>
                            Estado:
                            <span> @switch($us->estado)
                                @case('A')
                                    Activo
                                    @break
                                @case('I')
                                    Dado de Baja
                                    @break
                            @endswitch</span>
                        </div>
                    </div>
                    <div class="div_botones">
                        <a class="btnEditar" href="{{route('editarUsuario', $us->idUsuario)}}">Editar</a>
                        <input type="button" class="btnBaja" id="{{$us->idUsuario}}" name='{{$us->nombreUsuario}}' value="Dar de Baja" onclick="darDeBaja(this)">                        
                    </div>
                </div>
            @endforeach
        </div>
        <div id="div_fondoOscuro">
            <div id="div_contMensaje">
                
            </div>
        </div>
    </div>

@endsection