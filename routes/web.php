<?php

use App\Http\Controllers\ctr_login;
use App\Http\Controllers\ctr_usuarios;
use App\Http\Controllers\ctr_activos;
use App\Http\Controllers\ctr_instrumentos;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ctr_login::class)->name('inicio');

Route::post('/login', [ctr_login::class, 'ingresar'])->name('ingresar');

Route::get('/logout', [ctr_login::class, 'salir'])->name('salir');

//  Rutas necesarias para los usuarios

Route::get('/usuarios', ctr_usuarios::class)->name('usuarios'); //  Ruta para mostrar los usuarios

Route::get('/usuarios/agregar', [ctr_usuarios::class, 'agregarUsuario'])->name('agregarUsuario');   //  Ruta para ir a la pantalla de agregar usuarios

Route::post('/usuarios/agregar/guardar', [ctr_usuarios::class, 'guardarUsuario'])->name('guardarUsuario');  //  Ruta para guardar usuarios nuevos

Route::get('/usuarios/editar/{id}', [ctr_usuarios::class, 'editarUsuario'])->name('editarUsuario'); //  Ruta para ir a la pantalla de editar usuarios

Route::put('/usuarios/editar/guardar', [ctr_usuarios::class, 'actualizarUsuario'])->name('actualizarUsuario');  //  Ruta para actualizar un usuario editado

Route::get('/usuarios/darBaja/{id}', [ctr_usuarios::class, 'bajaUsuario'])->name('bajaUsuario');    //  Ruta para dar de baja a un usuario

//  Rutas necesarias para los activos

Route::get('/activos', ctr_activos::class)->name('activos');

Route::get('/activos/agregar', [ctr_activos::class, 'agregarActivo'])->name('agregarActivo');   //  Ruta para ir a la pantalla de agregar activos

Route::post('/activos/agregar/guardar',[ctr_activos::class, 'guardarActivo'])->name('guardarActivo');

Route::get('/activos/editar/{id}',[ctr_activos::class, 'goEditarActivo'])->name('goEditarActivo');

Route::put('/activos/editar/guardar',[ctr_activos::class, 'actualizarActivo'])->name('actualizarActivo');

Route::get('/activos/darBaja/{id}', [ctr_activos::class, 'bajaActivo'])->name('bajaActivo');    //  Ruta para dar de baja un activo

//  Rutas necesarias para los instrumentos

Route::get('/instrumentos', ctr_instrumentos::class)->name('instrumentos');